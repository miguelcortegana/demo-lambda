package com.gsifactory.demolambda.controller;

import com.gsifactory.demolambda.entities.People;
import com.gsifactory.demolambda.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class PeopleRestController {

    private PeopleService peopleService;

    public PeopleRestController(@Autowired PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @GetMapping(value = "/people/all",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllPeople() {
        var listOfPeople = peopleService.getAllPeople();
        if (!listOfPeople.isEmpty()) {
            return ResponseEntity.ok(listOfPeople);
        } else {
            return new ResponseEntity<>("No people data registered", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/people",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findPeopleByName(@RequestParam(name = "name") String name) {
        var listOfPeople = peopleService.findPeopleByName(name);
        if (!listOfPeople.isEmpty())
            return new ResponseEntity<>(listOfPeople, HttpStatus.OK);
        else
            return new ResponseEntity<>("No found people with name [" + name + "]", HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/people/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findPeopleById(@PathVariable(name = "id") Long id) {
        var people = peopleService.findPeopleById(id);
        if (people != null)
            return new ResponseEntity<>(people, HttpStatus.OK);
        else
            return new ResponseEntity<>("No people found with id = [" + id + "]", HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/people",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveNewPeople(@RequestBody People people) {
        peopleService.addPeople(people);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/people",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePeopleData(@RequestBody People people) {
        if (people.getId() == null || people.getId() <= 0)
            return new ResponseEntity<>("People must have an id", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>("People data updated", HttpStatus.OK);
    }

    @DeleteMapping(value = "/people/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deletePeopleById(@PathVariable(name = "id") Long id) {
        var people = peopleService.findPeopleById(id);
        if (people != null) {
            peopleService.deletePeople(id);
            return new ResponseEntity<>("People deleted", HttpStatus.OK);
        } else
            return new ResponseEntity<>("No people found with id = [" + id + "]", HttpStatus.NOT_FOUND);
    }
}
