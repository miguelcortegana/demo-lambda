package com.gsifactory.demolambda.service.impl;

import com.gsifactory.demolambda.entities.People;
import com.gsifactory.demolambda.repository.PeopleRepository;
import com.gsifactory.demolambda.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleServiceImpl implements PeopleService {

    private PeopleRepository peopleRepository;

    public PeopleServiceImpl(@Autowired PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public List<People> getAllPeople() {
        return peopleRepository.findAll();
    }

    @Override
    public People findPeopleById(Long id) {
        return peopleRepository.findById(id).orElse(null);
    }

    @Override
    public List<People> findPeopleByName(String name) {
        return peopleRepository.findByName(name);
    }

    @Override
    public void addPeople(People people) {
        peopleRepository.save(people);
    }

    @Override
    public void deletePeople(Long id) {
        peopleRepository.deleteById(id);
    }
}
