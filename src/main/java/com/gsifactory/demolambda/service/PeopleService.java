package com.gsifactory.demolambda.service;

import com.gsifactory.demolambda.entities.People;

import java.util.List;

public interface PeopleService {

    public List<People> getAllPeople();

    public People findPeopleById(Long id);

    public List<People> findPeopleByName(String name);

    public void addPeople(People people);

    public void deletePeople(Long id);

}
