package com.gsifactory.demolambda.repository;

import com.gsifactory.demolambda.entities.People;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleRepository extends JpaRepository<People, Long> {

    public List<People> findByName(String name);

}
